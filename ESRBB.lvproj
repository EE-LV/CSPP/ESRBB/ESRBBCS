﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Property Name="CCSymbols" Type="Str">CSPP_BuildContent,CSPP_Core;CSPP_WebpubLaunchBrowser,None;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str">This LabVIEW project is used to develop this application based on NI ActorFramework and CS++ libraries.

This ESRBB.lvproj is used to develop an application based on NI ActorFramework and CS++ libraries for barrier-bucket cavity control. Published under EUPL.

Please refer also to README.md.

Author: H.Brand@gsi.de

Copyright 2020  GSI Helmholtzzentrum für Schwerionenforschung GmbH

EEL, Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="varPersistentID:{02DF70EC-16D5-4A93-B039-9BC6BD780BD5}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_PollingInterval</Property>
	<Property Name="varPersistentID:{04AEA2A6-AD77-448A-896A-F4FE87407003}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Usage_C</Property>
	<Property Name="varPersistentID:{04DE4097-22D0-4282-A580-2A97C03FF7B4}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActorProxy_Activate</Property>
	<Property Name="varPersistentID:{05278D53-10F6-4317-9EE4-E4295AECFA78}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/SystemMonitor_PollingTime</Property>
	<Property Name="varPersistentID:{05900173-ADAF-44C4-BFE3-3A40F7599379}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{068BE5DB-18F1-4B3E-B92F-92F56E1C1E68}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_Error</Property>
	<Property Name="varPersistentID:{092F5467-60FC-4B52-AE27-AAA95AEE6B86}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_PollingInterval</Property>
	<Property Name="varPersistentID:{0A968C8E-F525-4B4C-A837-C1376774631D}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_ErrorCode</Property>
	<Property Name="varPersistentID:{0C85DE14-7E6F-4D6C-9397-F4F3414C7C2E}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/Beep_PollingTime</Property>
	<Property Name="varPersistentID:{0D3873B8-F867-46B8-8543-88B9EC57CAF0}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_ErrorStatus</Property>
	<Property Name="varPersistentID:{0E185549-05B6-4895-AB59-65F6EBF47C4C}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{11BFDD8F-A732-40F2-A0DD-E66B25BF47DA}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Free_C</Property>
	<Property Name="varPersistentID:{12B542A5-ECA7-4D3A-B20D-0DCB9FA56701}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingTime</Property>
	<Property Name="varPersistentID:{136FBC7E-62A0-454C-9F38-03909BA67F79}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_ErrorMessage</Property>
	<Property Name="varPersistentID:{14EB8D35-5B7B-40FE-84A7-279CCCE175C7}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/ObjectManager_PollingDeltaT</Property>
	<Property Name="varPersistentID:{15409A2B-E0B9-4761-97FC-29B30925CC34}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingIterations</Property>
	<Property Name="varPersistentID:{1C4045FF-E96F-4D84-8145-4C0489A34CE9}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/SystemMonitor_Initialized</Property>
	<Property Name="varPersistentID:{2003F9A3-58B1-4FC2-81F9-31E375046770}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/BeepProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{21C8EA43-B267-457D-8F90-A116CFA5A229}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/Beep_PollingCounter</Property>
	<Property Name="varPersistentID:{2313793E-11D9-479A-AD22-728634899295}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/Beep_PollingInterval</Property>
	<Property Name="varPersistentID:{239C8B85-F6B3-4F31-8EAC-346DAF19204E}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/ObjectManager_Initialized</Property>
	<Property Name="varPersistentID:{25B8C2B7-E23F-4C96-AA1D-74FE26220925}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingInterval</Property>
	<Property Name="varPersistentID:{2804F86D-6653-4518-9624-3C887F9BC517}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_PollingIterations</Property>
	<Property Name="varPersistentID:{28936C09-F148-42F3-B2FC-5BA0057C3627}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_PollingCounter</Property>
	<Property Name="varPersistentID:{2974F87C-5D3A-4C42-8B8F-7A62048C68FA}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_PollingDeltaT</Property>
	<Property Name="varPersistentID:{2EB1B1A0-3977-4D57-9475-B689699DC497}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_PollingMode</Property>
	<Property Name="varPersistentID:{2F68CDCF-BF16-492E-9E11-A57FCBF96656}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/WatchdogProxy_Activate</Property>
	<Property Name="varPersistentID:{30E3AD5A-D8EE-4B27-86B4-07E95AC0DE15}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/SystemMonitor_PollingInterval</Property>
	<Property Name="varPersistentID:{31A4A58F-F5C6-40F7-8F4B-C362C215FA4F}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{3272D0B6-9ADD-4909-919B-9D5160FE209C}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingDeltaT</Property>
	<Property Name="varPersistentID:{348045BF-8344-444F-9EB2-23EB48E57AAC}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Error</Property>
	<Property Name="varPersistentID:{35EEE3CF-39C1-464F-AB8E-E98313A8229B}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/BeepProxy_Activate</Property>
	<Property Name="varPersistentID:{3737871D-CB34-404D-A6CF-904B33F99D1B}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_PollingTime</Property>
	<Property Name="varPersistentID:{37467BF2-E30D-47CA-90AD-BCA0427EC698}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_PollingTime</Property>
	<Property Name="varPersistentID:{38E23607-087A-4CAE-9DDC-6C8C8E971CD1}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/ActorList</Property>
	<Property Name="varPersistentID:{39FFA8EE-1D7E-414B-873E-A7B98AFDCB50}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_ErrorStatus</Property>
	<Property Name="varPersistentID:{3BE2EAC3-305E-40A6-AB32-4594E4F60463}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{428C60B4-F52C-4777-80A2-FE09E46BC9D9}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_PollingInterval</Property>
	<Property Name="varPersistentID:{46376221-9BE7-42CC-88E5-81C14FFB0D75}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_PollingDeltaT</Property>
	<Property Name="varPersistentID:{49126F1F-7C23-435B-A5DA-0265AEB5F8E5}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/ObjectManagerProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{4F1836D6-B21B-4EF5-ADB7-887C333E4362}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/ObjectManager_ErrorCode</Property>
	<Property Name="varPersistentID:{52CA4CA2-96E3-4EBE-8B65-77E2C2EA884C}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{531C2992-4EE1-401B-A0EA-CE7FE963043E}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/ObjectManager_PollingIterations</Property>
	<Property Name="varPersistentID:{53AE040A-758D-490F-8598-6BC391873622}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_PollingMode</Property>
	<Property Name="varPersistentID:{53FB95E3-4DAE-4A02-8ABB-32A6A1451E3E}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManagerProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{58478C2F-BDEA-42C1-83AA-6AFDDBBB497C}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{5BE91F73-120A-4B38-B2A2-DA4442EFABEB}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_PollingMode</Property>
	<Property Name="varPersistentID:{5F88DDAB-CD2F-4D03-88F4-6D61588C0E7C}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{5FEC2A33-E3F7-49FA-8EB8-273BEAE31DF4}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{61F8DFF5-E810-4FD2-912B-9A9E899B5587}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_SelftestResultCode</Property>
	<Property Name="varPersistentID:{628C00D2-59EC-4E6D-8117-C71B347F8CCD}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_ErrorMessage</Property>
	<Property Name="varPersistentID:{6364A3FB-3378-4E8B-B10B-BDD0D3F08F0B}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/ObjectManagerProxy_Activate</Property>
	<Property Name="varPersistentID:{6423DFA3-7F39-4F9D-8ADA-C65DB4306040}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/SystemMonitorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{660E98BC-356F-423A-BEC9-2FA5A43399E3}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_ErrorStatus</Property>
	<Property Name="varPersistentID:{6ACC9C87-7484-4557-B253-9E909D183858}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/ObjectManager_PollingTime</Property>
	<Property Name="varPersistentID:{6D5053F7-B277-4C37-AA22-7251DEABB4F9}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_ErrorCode</Property>
	<Property Name="varPersistentID:{6DFB121F-1BED-45F9-963D-4AD1C9085907}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/BeepProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{70F0FD87-1F31-46C6-AD18-38B1A2A72E4B}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_PollingIterations</Property>
	<Property Name="varPersistentID:{717539FA-EEFF-44D2-94E4-575AC7D6F32D}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{7289D7D7-8CA9-4FA7-ABD7-D130EE14C280}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/SystemMonitor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{7684B1A2-EC41-455A-AE68-519ED732BB23}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/ObjectManager_ErrorMessage</Property>
	<Property Name="varPersistentID:{773D6543-5404-44AF-9CDF-1412D2C87C0D}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_CPU-Load</Property>
	<Property Name="varPersistentID:{77E30E2B-3303-44A4-8982-499BE2B89894}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_DriverRevision</Property>
	<Property Name="varPersistentID:{784F9E83-1F33-4D40-97FC-F8CA27FB9505}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingTime</Property>
	<Property Name="varPersistentID:{79145E0A-4940-441F-9BFE-32787FDFDADF}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{79A913E0-30B6-4E9F-BDA4-8D12B39D9599}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{79EEDD72-5C02-42D7-A917-40EBE4B8FBB6}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/BeepProxy_Activate</Property>
	<Property Name="varPersistentID:{7A88566C-7A6B-4560-B643-EDBAE31F763C}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_ErrorCode</Property>
	<Property Name="varPersistentID:{805C50EF-0665-448B-9D5D-5DD7263B06C9}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingMode</Property>
	<Property Name="varPersistentID:{8207A06F-68CC-4FDD-B6A3-814BD0802287}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManagerProxy_Activate</Property>
	<Property Name="varPersistentID:{8265EFE7-B259-436E-85CB-CCE3B871024A}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitorProxy_Activate</Property>
	<Property Name="varPersistentID:{82B110FE-C57E-477B-BD26-38F22853F66D}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{83613C9A-793C-4E7C-91C1-6EF58345B2D5}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_PollingInterval</Property>
	<Property Name="varPersistentID:{83FA732A-957F-4F2F-B641-21D5A3AB3F05}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_ErrorStatus</Property>
	<Property Name="varPersistentID:{851C8B7C-1B88-44FE-90E9-7A09C1AFDE39}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_PollingIterations</Property>
	<Property Name="varPersistentID:{852D4F5F-540B-4B18-B4E4-70473CF2F39F}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_PollingTime</Property>
	<Property Name="varPersistentID:{8FC4EE26-9F76-4CC4-BE78-E7C7E71F81DE}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/SystemMonitor_CPU-Load</Property>
	<Property Name="varPersistentID:{90862C11-B429-4C59-8C74-46938A52A686}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/Beep_ErrorStatus</Property>
	<Property Name="varPersistentID:{96A464D6-A32F-4BF5-9FAC-D587F06E4DF3}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/SystemMonitor_Usage_C</Property>
	<Property Name="varPersistentID:{9849132E-3BAA-4903-BFC5-CA0C54A46DC3}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_Error</Property>
	<Property Name="varPersistentID:{9D3F8C9E-4A4C-4D0E-BD73-9220474E38B1}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingCounter</Property>
	<Property Name="varPersistentID:{9FC2D8EF-2165-48E1-8259-1F9C8FBF9445}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/ObjectManager_ErrorStatus</Property>
	<Property Name="varPersistentID:{A156E4DA-9254-4A7B-BF07-8C6C9F862F18}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/SystemMonitor_PollingIterations</Property>
	<Property Name="varPersistentID:{A3230CD1-D40D-4B28-9EB6-E916815C4388}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_PollingTime</Property>
	<Property Name="varPersistentID:{A3E30524-394C-43B8-9BCF-29BBD2A3AA67}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingIterations</Property>
	<Property Name="varPersistentID:{A42E3DC5-28C7-42B3-A6EC-83D4686C34CE}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{A44347E7-F0EF-4BA0-9F0A-A6B5BB3283DC}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{A6A6F961-8532-4FA0-8D04-7EA4E5E4BD22}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/Beep_Initialized</Property>
	<Property Name="varPersistentID:{A8BEA20A-062D-4950-A155-8E3600C7C2E4}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/ObjectManager_PollingMode</Property>
	<Property Name="varPersistentID:{A9713CBF-0D51-45FD-B152-63BAE649359D}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{AA0B813A-F361-47C5-9BA4-86063B119291}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/SystemMonitor_PollingMode</Property>
	<Property Name="varPersistentID:{AA8875D8-FF0E-4470-A75A-27E5ED484CA5}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/SystemMonitor_Size_C</Property>
	<Property Name="varPersistentID:{ABCB73C6-419A-4236-B78C-BE076A7FBB5B}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/SystemMonitor_ErrorCode</Property>
	<Property Name="varPersistentID:{AC2E5CB3-6DD5-4ABD-A92D-7EAF74B65199}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Initialized</Property>
	<Property Name="varPersistentID:{B0B1D76E-EDEE-4963-B0D6-7484D68B2C44}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_ErrorStatus</Property>
	<Property Name="varPersistentID:{B1BEAD85-C461-4469-85CF-F2EE7F03EA65}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/SystemMonitor_ErrorMessage</Property>
	<Property Name="varPersistentID:{B2FEAD6A-0510-4E96-B447-B07BAD97FAB4}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{B5FF42EC-8C09-4EB4-B91F-BE3698737C60}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_Error</Property>
	<Property Name="varPersistentID:{B7F0C64A-CCF4-47A6-A08C-05EA2F622427}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_SelfTest</Property>
	<Property Name="varPersistentID:{B9AEE801-68CF-478E-8959-60CDAD82D699}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/Beep_PollingIterations</Property>
	<Property Name="varPersistentID:{BA108D45-6F87-450E-8DB8-C75AE3AE0C6B}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/Beep_PollingMode</Property>
	<Property Name="varPersistentID:{BA76AF83-365C-4641-B248-E39AAD017AE2}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/Beep_ErrorMessage</Property>
	<Property Name="varPersistentID:{BBE7CB13-DC1E-4D27-97DF-F6F55FB2D769}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_ErrorCode</Property>
	<Property Name="varPersistentID:{BDFF2152-3DC0-4F19-AE9A-4973F4BB8BA0}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingInterval</Property>
	<Property Name="varPersistentID:{BEA96B25-36D7-4999-B215-EDEA8FEFC3CB}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/ObjectManager_PollingCounter</Property>
	<Property Name="varPersistentID:{C2DB8314-44B5-4A4D-AE05-2EE191404CEC}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/ObjectManager_Error</Property>
	<Property Name="varPersistentID:{C41487E2-9206-421B-A943-61A823919B4F}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/SystemMonitorProxy_Activate</Property>
	<Property Name="varPersistentID:{C7CCB3EC-466D-49F9-9199-237904AE351D}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{C7D64B5A-9C34-49B9-B595-68057D6EF236}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/SystemMonitor_Memory</Property>
	<Property Name="varPersistentID:{C80423CD-CB13-4E88-8580-274252136563}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_Error</Property>
	<Property Name="varPersistentID:{C867D9EF-9138-4955-BC78-E3EEC46358B0}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_ResourceName</Property>
	<Property Name="varPersistentID:{CA0694D0-43D6-4D13-B0E8-D562290EE8FD}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/WatchdogProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{CFF35AC0-73A1-4CFE-A678-C4C62D407BDF}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/Beep_ErrorCode</Property>
	<Property Name="varPersistentID:{D195E3EA-2FC8-4FA7-A661-51F1C20CF7C6}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_PollingCounter</Property>
	<Property Name="varPersistentID:{D2D27A67-B5A3-4D17-AF69-38E80056AA97}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_PollingCounter</Property>
	<Property Name="varPersistentID:{D332EE0F-CE9A-4E4A-B94D-2C61ECCC3305}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Reset</Property>
	<Property Name="varPersistentID:{D3772573-6B4F-4F66-8B19-B5A8CA087196}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_PollingCounter</Property>
	<Property Name="varPersistentID:{D872A362-36F4-4476-95ED-0DF0A6E2D8DF}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/SystemMonitor_PollingCounter</Property>
	<Property Name="varPersistentID:{D9520540-D340-47DB-938B-D1FB165ACCA6}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_ErrorCode</Property>
	<Property Name="varPersistentID:{D971C09B-5BB5-429E-9007-104E76ED1508}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Error</Property>
	<Property Name="varPersistentID:{DB8F4682-39FD-4CD0-A3B3-45B595BDFB2A}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingCounter</Property>
	<Property Name="varPersistentID:{DE8B0C0D-78FA-452B-9727-4BF59FAFCA9A}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActorProxy_Activate</Property>
	<Property Name="varPersistentID:{E0188F8D-FDFC-4E52-8A8D-C95463511DE2}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_FirmwareRevision</Property>
	<Property Name="varPersistentID:{E0C74B09-8FB6-4193-8FCB-0F9B8B5A97DE}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{E27F1048-F66F-41EF-9B3E-901724E0A595}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_ErrorMessage</Property>
	<Property Name="varPersistentID:{E2F10D67-1927-4AE7-B331-D7D6673E5D05}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/SystemMonitor_Error</Property>
	<Property Name="varPersistentID:{E307218C-FF59-4935-9E0E-763432F8E02A}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{E3225A3B-77E4-4DA9-81B1-DBA13C1EDD59}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/Beep_PollingDeltaT</Property>
	<Property Name="varPersistentID:{E521B3CF-38DD-48C3-8609-FCCB6C3F1625}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_ErrorMessage</Property>
	<Property Name="varPersistentID:{E55EDF91-5622-4701-80E1-DA4EA0101251}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_ErrorMessage</Property>
	<Property Name="varPersistentID:{E712F7C0-C5EB-4138-B871-E06BE14CC01D}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_PollingMode</Property>
	<Property Name="varPersistentID:{E7BEE090-AE98-4C30-9CBB-61896182F8EE}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{E7DF3CA3-5817-44E5-9833-B898766A5AD2}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Memory</Property>
	<Property Name="varPersistentID:{EDC36392-FED5-4D0C-8DF9-3AFFED434D85}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Size_C</Property>
	<Property Name="varPersistentID:{EFB75177-BE8E-4109-BB11-ABDAC862D7BB}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{F0132FC9-B24E-40AD-8521-2F3024874499}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/SystemMonitor_Free_C</Property>
	<Property Name="varPersistentID:{F1068361-6888-476C-98D6-43533FA245BC}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_ErrorCode</Property>
	<Property Name="varPersistentID:{F284C4B2-512A-485D-8150-9CF7DE7A2ED5}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_PollingIterations</Property>
	<Property Name="varPersistentID:{F60841D5-A748-4F70-A65B-EE98F20B89C2}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_ErrorStatus</Property>
	<Property Name="varPersistentID:{F6B80B5D-C71D-4FC6-A5E4-1E2FC3938ECF}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingMode</Property>
	<Property Name="varPersistentID:{F725B362-2569-4BB7-95A6-F81F6A31BD17}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_Initialized</Property>
	<Property Name="varPersistentID:{FA63A55E-FD8D-4AA0-ACB2-698B95D2C7D7}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/ObjectManager_PollingInterval</Property>
	<Property Name="varPersistentID:{FC453073-D0AA-411C-99F7-C37CFA236425}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{FCF72D76-A50E-4483-A590-83B1225257F9}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_ErrorMessage</Property>
	<Property Name="varPersistentID:{FEB91004-BAF4-4E9C-A4ED-92BF6D2BCEC6}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/SystemMonitor_ErrorStatus</Property>
	<Property Name="varPersistentID:{FFDCA039-9115-4D32-BF5E-1E6A85399080}" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib/Beep_Error</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="AF" Type="Folder">
			<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Report Error Msg/Report Error Msg.lvclass"/>
			<Item Name="Self-Addressed Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Self-Addressed Msg/Self-Addressed Msg.lvclass"/>
		</Item>
		<Item Name="Documentation" Type="Folder"/>
		<Item Name="EUPL License" Type="Folder">
			<Item Name="EUPL v.1.1 - Lizenz.pdf" Type="Document" URL="../EUPL v.1.1 - Lizenz.pdf"/>
			<Item Name="EUPL v.1.1 - Lizenz.rtf" Type="Document" URL="../EUPL v.1.1 - Lizenz.rtf"/>
		</Item>
		<Item Name="instr.lib" Type="Folder"/>
		<Item Name="Packages" Type="Folder">
			<Item Name="Core" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Item Name="CSPP_BaseActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_BaseActor/CSPP_BaseActor.lvlib"/>
					<Item Name="CSPP_DeviceActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceActor/CSPP_DeviceActor.lvlib"/>
					<Item Name="CSPP_DeviceGUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceGUIActor/CSPP_DeviceGUIActor.lvlib"/>
					<Item Name="CSPP_DSMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DSMonitor/CSPP_DSMonitor.lvlib"/>
					<Item Name="CSPP_GUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_GUIActor/CSPP_GUIActor.lvlib"/>
					<Item Name="CSPP_LMMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_LMMonitor/CSPP_LMMonitor.lvlib"/>
					<Item Name="CSPP_LNMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_LNMonitor/CSPP_LNMonitor.lvlib"/>
					<Item Name="CSPP_PVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVMonitor/CSPP_PVMonitor.lvlib"/>
					<Item Name="CSPP_PVProxy.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVProxy/CSPP_PVProxy.lvlib"/>
					<Item Name="CSPP_PVSubscriber.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVSubscriber/CSPP_PVSubscriber.lvlib"/>
					<Item Name="CSPP_QMsgLogMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_QMsgLogMonitor/CSPP_QMsgLogMonitor.lvlib"/>
					<Item Name="CSPP_StartActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_StartActor/CSPP_StartActor.lvlib"/>
					<Item Name="CSPP_SVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_SVMonitor/CSPP_SVMonitor.lvlib"/>
					<Item Name="CSPP_TDMSStorage.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_TDMSStorage/CSPP_TDMSStorage.lvlib"/>
					<Item Name="CSPP_Watchdog.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_Watchdog/CSPP_Watchdog.lvlib"/>
				</Item>
				<Item Name="Classes" Type="Folder">
					<Item Name="CSPP_BaseClasses.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_BaseClasses/CSPP_BaseClasses.lvlib"/>
					<Item Name="CSPP_ProcessVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/CSPP_ProcessVariables.lvlib"/>
					<Item Name="CSPP_SharedVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/SVConnection/CSPP_SharedVariables.lvlib"/>
				</Item>
				<Item Name="Documentation" Type="Folder">
					<Item Name="Change_Log.txt" Type="Document" URL="../Packages/CSPP_Core/Change_Log.txt"/>
					<Item Name="Release_Notes.txt" Type="Document" URL="../Packages/CSPP_Core/Release_Notes.txt"/>
					<Item Name="VI-Analyzer-Configuration.cfg" Type="Document" URL="../Packages/CSPP_Core/VI-Analyzer-Configuration.cfg"/>
					<Item Name="VI-Analyzer-Results.rsl" Type="Document" URL="../Packages/CSPP_Core/VI-Analyzer-Results.rsl"/>
					<Item Name="VI-Analyzer-Spelling-Exceptions.txt" Type="Document" URL="../Packages/CSPP_Core/VI-Analyzer-Spelling-Exceptions.txt"/>
				</Item>
				<Item Name="Libraries" Type="Folder">
					<Item Name="CSPP_Base.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Base/CSPP_Base.lvlib"/>
					<Item Name="CSPP_Utilities.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Utilities/CSPP_Utilities.lvlib"/>
				</Item>
				<Item Name="Messages" Type="Folder">
					<Item Name="CSPP_AEUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AEUpdate Msg/CSPP_AEUpdate Msg.lvlib"/>
					<Item Name="CSPP_AsyncCallbackMsg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AsyncCallbackMsg/CSPP_AsyncCallbackMsg.lvlib"/>
					<Item Name="CSPP_DataUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_DataUpdate Msg/CSPP_DataUpdate Msg.lvlib"/>
					<Item Name="CSPP_NAInitialized Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_NAInitialized Msg/CSPP_NAInitialized Msg.lvlib"/>
					<Item Name="CSPP_PVUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_PVUpdate Msg/CSPP_PVUpdate Msg.lvlib"/>
					<Item Name="CSPP_Watchdog Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_Watchdog Msg/CSPP_Watchdog Msg.lvlib"/>
				</Item>
				<Item Name="CSPP_Core-errors.txt" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core-errors.txt"/>
				<Item Name="CSPP_Core.ini" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core.ini"/>
				<Item Name="CSPP_CoreContent-Linux.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreContent-Linux.vi"/>
				<Item Name="CSPP_CoreContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreContent.vi"/>
				<Item Name="CSPP_CoreGUIContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreGUIContent.vi"/>
				<Item Name="CSPP_DeploymentExample.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_DeploymentExample.vi"/>
				<Item Name="CSPP_Post-Build Action.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_Post-Build Action.vi"/>
			</Item>
			<Item Name="DSC" Type="Folder">
				<Item Name="CSPP_DSC.ini" Type="Document" URL="../Packages/CSPP_DSC/CSPP_DSC.ini"/>
				<Item Name="CSPP_DSCAlarmViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCAlarmViewer/CSPP_DSCAlarmViewer.lvlib"/>
				<Item Name="CSPP_DSCConnection.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/DSCConnection/CSPP_DSCConnection.lvlib"/>
				<Item Name="CSPP_DSCContent.vi" Type="VI" URL="../Packages/CSPP_DSC/CSPP_DSCContent.vi"/>
				<Item Name="CSPP_DSCManager.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCManager/CSPP_DSCManager.lvlib"/>
				<Item Name="CSPP_DSCMonitor.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCMonitor/CSPP_DSCMonitor.lvlib"/>
				<Item Name="CSPP_DSCMsgLogger.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/CSPP_DSCMsgLogger/CSPP_DSCMsgLogger.lvlib"/>
				<Item Name="CSPP_DSCTrendViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCTrendViewer/CSPP_DSCTrendViewer.lvlib"/>
				<Item Name="CSPP_DSCUtilities.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Libs/CSPP_DSCUtilities/CSPP_DSCUtilities.lvlib"/>
				<Item Name="DSC Remote SV Access.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Contributed/DSC Remote SV Access.lvlib"/>
			</Item>
			<Item Name="ObjectManager" Type="Folder">
				<Item Name="CSPP_ObjectManager.ini" Type="Document" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.ini"/>
				<Item Name="CSPP_ObjectManager.lvlib" Type="Library" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.lvlib"/>
				<Item Name="CSPP_ObjectManager_Content.vi" Type="VI" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager_Content.vi"/>
				<Item Name="CSPP_ObjectManager_SV.lvlib" Type="Library" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager_SV.lvlib"/>
			</Item>
			<Item Name="Utilities" Type="Folder">
				<Item Name="CSPP_BeepActor.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_BeepActor/CSPP_BeepActor.lvlib"/>
				<Item Name="CSPP_SystemMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_SystemMonitor/CSPP_SystemMonitor.lvlib"/>
				<Item Name="CSPP_SystemMonitor_SV.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/CSPP_SystemMonitor_SV.lvlib"/>
				<Item Name="CSPP_Utilities.ini" Type="Document" URL="../Packages/CSPP_Utilities/CSPP_Utilities.ini"/>
				<Item Name="CSPP_Utilities_SV.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/CSPP_Utilities_SV.lvlib"/>
				<Item Name="CSPP_UtilitiesContent.vi" Type="VI" URL="../Packages/CSPP_Utilities/CSPP_UtilitiesContent.vi"/>
			</Item>
		</Item>
		<Item Name="SV.lib" Type="Folder">
			<Item Name="CSPP_Core_SV.lvlib" Type="Library" URL="../Packages/CSPP_Core/CSPP_Core_SV.lvlib"/>
			<Item Name="ESRBB.lvlib" Type="Library" URL="../SV.lib/ESRBB.lvlib"/>
		</Item>
		<Item Name="User" Type="Folder"/>
		<Item Name="CSPP-Minimum.ini" Type="Document" URL="../CSPP-Minimum.ini"/>
		<Item Name="ESRBB.ini" Type="Document" URL="../ESRBB.ini"/>
		<Item Name="ESRBB_Content.vi" Type="VI" URL="../ESRBB_Content.vi"/>
		<Item Name="ESRBB_Main.vi" Type="VI" URL="../ESRBB_Main.vi"/>
		<Item Name="README.md" Type="Document" URL="../README.md"/>
		<Item Name="Release_Notes.md" Type="Document" URL="../Release_Notes.md"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="1D String Array to Delimited String.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/1D String Array to Delimited String.vi"/>
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="ALM_Clear_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Clear_UD_Alarm.vi"/>
				<Item Name="ALM_Error_Resolve.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Error_Resolve.vi"/>
				<Item Name="ALM_Get_Alarms.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_Alarms.vi"/>
				<Item Name="ALM_Get_User_Name.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_User_Name.vi"/>
				<Item Name="ALM_GetTagURLs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_GetTagURLs.vi"/>
				<Item Name="ALM_Set_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Alarm.vi"/>
				<Item Name="ALM_Set_UD_Event.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Event.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Beep.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/Beep.vi"/>
				<Item Name="BuildErrorSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="cfis_Get File Extension Without Changing Case.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/cfis_Get File Extension Without Changing Case.vi"/>
				<Item Name="cfis_Replace Percent Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/cfis_Replace Percent Code.vi"/>
				<Item Name="cfis_Reverse Scan From String For Integer.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/cfis_Reverse Scan From String For Integer.vi"/>
				<Item Name="cfis_Split File Path Into Three Parts.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/cfis_Split File Path Into Three Parts.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Check Whether Timeouted.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/Check Whether Timeouted.vi"/>
				<Item Name="citadel_ConvertDatabasePathToName.vi" Type="VI" URL="/&lt;vilib&gt;/citadel/citadel_ConvertDatabasePathToName.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create File with Incrementing Suffix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Create File with Incrementing Suffix.vi"/>
				<Item Name="CreateOrAddLibraryToParent.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToParent.vi"/>
				<Item Name="CreateOrAddLibraryToProject.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToProject.vi"/>
				<Item Name="CTL_defaultProcessName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_defaultProcessName.vi"/>
				<Item Name="Delimited String to 1D String Array.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Delimited String to 1D String Array.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="dsc_PrefsPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/info/dsc_PrefsPath.vi"/>
				<Item Name="dscCommn.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/common/dscCommn.dll"/>
				<Item Name="dscProc.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/process/dscProc.dll"/>
				<Item Name="ERR_ErrorClusterFromErrorCode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_ErrorClusterFromErrorCode.vi"/>
				<Item Name="ERR_GetErrText.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_GetErrText.vi"/>
				<Item Name="ERR_MergeErrors.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_MergeErrors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="FileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
				<Item Name="FileVersionInformation.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FindCloseTagByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindCloseTagByName.vi"/>
				<Item Name="FindElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElement.vi"/>
				<Item Name="FindElementStartByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElementStartByName.vi"/>
				<Item Name="FindEmptyElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindEmptyElement.vi"/>
				<Item Name="FindFirstTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindFirstTag.vi"/>
				<Item Name="FindMatchingCloseTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindMatchingCloseTag.vi"/>
				<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetFileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
				<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="HIST_FormatTagname&amp;ProcessFilterSpec.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_FormatTagname&amp;ProcessFilterSpec.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="MoveMemory.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
				<Item Name="NET_GetHostName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_GetHostName.vi"/>
				<Item Name="NET_IsComputerLocalhost.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_IsComputerLocalhost.vi"/>
				<Item Name="NET_localhostToMachineName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_localhostToMachineName.vi"/>
				<Item Name="NET_resolveNVIORef.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_resolveNVIORef.vi"/>
				<Item Name="NET_SameMachine.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_SameMachine.vi"/>
				<Item Name="NET_tagURLdecode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_tagURLdecode.vi"/>
				<Item Name="ni_citadel_lv.dll" Type="Document" URL="/&lt;vilib&gt;/citadel/ni_citadel_lv.dll"/>
				<Item Name="NI_DSC.lvlib" Type="Library" URL="/&lt;vilib&gt;/lvdsc/NI_DSC.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="ni_logos_BuildURL.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_BuildURL.vi"/>
				<Item Name="ni_logos_ValidatePSPItemName.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_ValidatePSPItemName.vi"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_Security Domain.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Domain.ctl"/>
				<Item Name="NI_Security Get Domains.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Get Domains.vi"/>
				<Item Name="NI_Security Identifier.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Identifier.ctl"/>
				<Item Name="NI_Security Resolve Domain.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Resolve Domain.vi"/>
				<Item Name="NI_Security_GetTimeout.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_GetTimeout.vi"/>
				<Item Name="NI_Security_ProgrammaticLogin.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ProgrammaticLogin.vi"/>
				<Item Name="NI_Security_ResolveDomainID.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ResolveDomainID.vi"/>
				<Item Name="NI_Security_ResolveDomainName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ResolveDomainName.vi"/>
				<Item Name="ni_security_salapi.dll" Type="Document" URL="/&lt;vilib&gt;/Platform/security/ni_security_salapi.dll"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="ni_tagger_lv_NewFolder.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_NewFolder.vi"/>
				<Item Name="ni_tagger_lv_ReadVariableConfig.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_ReadVariableConfig.vi"/>
				<Item Name="NI_Variable.lvlib" Type="Library" URL="/&lt;vilib&gt;/variable/NI_Variable.lvlib"/>
				<Item Name="nialarms.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/nialarms.dll"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="ParseXMLFragments.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/ParseXMLFragments.vi"/>
				<Item Name="PRC_AdoptVarBindURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_AdoptVarBindURL.vi"/>
				<Item Name="PRC_CachedLibVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CachedLibVariables.vi"/>
				<Item Name="PRC_CommitMultiple.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CommitMultiple.vi"/>
				<Item Name="PRC_ConvertDBAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ConvertDBAttr.vi"/>
				<Item Name="PRC_CreateFolders.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateFolders.vi"/>
				<Item Name="PRC_CreateProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateProc.vi"/>
				<Item Name="PRC_CreateSubLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateSubLib.vi"/>
				<Item Name="PRC_CreateVar.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateVar.vi"/>
				<Item Name="PRC_DataType2Prototype.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DataType2Prototype.vi"/>
				<Item Name="PRC_DeleteLibraryItems.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteLibraryItems.vi"/>
				<Item Name="PRC_DeleteProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteProc.vi"/>
				<Item Name="PRC_Deploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Deploy.vi"/>
				<Item Name="PRC_DumpProcess.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpProcess.vi"/>
				<Item Name="PRC_DumpSharedVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpSharedVariables.vi"/>
				<Item Name="PRC_EnableAlarmLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableAlarmLogging.vi"/>
				<Item Name="PRC_EnableDataLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableDataLogging.vi"/>
				<Item Name="PRC_GetLibFromURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetLibFromURL.vi"/>
				<Item Name="PRC_GetMonadAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadAttr.vi"/>
				<Item Name="PRC_GetMonadList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadList.vi"/>
				<Item Name="PRC_GetProcList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcList.vi"/>
				<Item Name="PRC_GetProcSettings.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcSettings.vi"/>
				<Item Name="PRC_GetVarAndSubLibs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarAndSubLibs.vi"/>
				<Item Name="PRC_GetVarList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarList.vi"/>
				<Item Name="PRC_GroupSVs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GroupSVs.vi"/>
				<Item Name="PRC_IOServersToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_IOServersToLib.vi"/>
				<Item Name="PRC_MakeFullPathWithCurrentVIsCallerPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MakeFullPathWithCurrentVIsCallerPath.vi"/>
				<Item Name="PRC_MutipleDeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MutipleDeploy.vi"/>
				<Item Name="PRC_OpenOrCreateLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_OpenOrCreateLib.vi"/>
				<Item Name="PRC_ParseLogosURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ParseLogosURL.vi"/>
				<Item Name="PRC_ROSProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ROSProc.vi"/>
				<Item Name="PRC_SVsToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_SVsToLib.vi"/>
				<Item Name="PRC_Undeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Undeploy.vi"/>
				<Item Name="PSP Enumerate Network Items.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/PSP Enumerate Network Items.vi"/>
				<Item Name="PTH_ConstructCustomURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/path/PTH_ConstructCustomURL.vi"/>
				<Item Name="Read From XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(array).vi"/>
				<Item Name="Read From XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(string).vi"/>
				<Item Name="Read From XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="Subscribe All Local Processes.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/controls/Alarms and Events/internal/Subscribe All Local Processes.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="VerQueryValue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write to XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(array).vi"/>
				<Item Name="Write to XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(string).vi"/>
				<Item Name="Write to XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File.vi"/>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="lksock.dll" Type="Document" URL="lksock.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="logosbrw.dll" Type="Document" URL="/&lt;resource&gt;/logosbrw.dll"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="nitaglv.dll" Type="Document" URL="nitaglv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="NVIORef.dll" Type="Document" URL="NVIORef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="SCT Default Types.ctl" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Default Types.ctl"/>
			<Item Name="SCT Get LVRTPath.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get LVRTPath.vi"/>
			<Item Name="SCT Get Types.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get Types.vi"/>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="CSPP-Template" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{773E39E1-BBD6-4173-9460-6FE7C7522091}</Property>
				<Property Name="App_INI_GUID" Type="Str">{A41324B4-1D83-44D0-AB6D-155F8178B03C}</Property>
				<Property Name="App_INI_itemID" Type="Ref"></Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">1</Property>
				<Property Name="App_useFFRTE" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{95E968AE-FBA3-4AF4-B59D-842A4FF56C3A}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">CSPP-Template</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/D/builds/NI_AB_PROJECTNAME/App</Property>
				<Property Name="Bld_postActionVIID" Type="Ref">/My Computer/Packages/Core/CSPP_Post-Build Action.vi</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{6211658F-9962-4EFC-81EE-AF45E75227E4}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_userLogFile" Type="Path">/D/builds/CSPP-Template/App/CSPP-Template_log.txt</Property>
				<Property Name="Destination[0].destName" Type="Str">CSPP-Template.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/D/builds/NI_AB_PROJECTNAME/App/CSPP-Template.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/D/builds/NI_AB_PROJECTNAME/App/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Exe_VardepHideDeployDlg" Type="Bool">true</Property>
				<Property Name="Exe_VardepUndeployOnExit" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{B474D4AD-F621-46B3-9DF4-EBC7B4A95452}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/SV.lib/ESRBB.lvlib</Property>
				<Property Name="Source[10].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[10].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[10].type" Type="Str">Library</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/README.md</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/ESRBB_Main.vi</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[3].type" Type="Str">VI</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Release_Notes.md</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref"></Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/Packages/DSC/CSPP_DSC.ini</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager.ini</Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities.ini</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/Packages/Core/CSPP_Core.ini</Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">11</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">CSPP-Template</Property>
				<Property Name="TgtF_internalName" Type="Str">CSPP-Template</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2017 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">CSPP-Template</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{20017911-8940-4421-B57F-6FCEEB0905B6}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">CSPP-Template.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
