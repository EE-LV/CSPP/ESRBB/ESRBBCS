ESRBB README
====================
_ESRBB.lvproj_ is used to develop an application based on NI ActorFramework and CS++ libraries. Currently used development SW is LabVIEW 2019.

This ESRBB.lvproj is used to develop an application based on NI ActorFramework and CS++ libraries for barrier-bucket cavity control.

GSI - Arbeitsnotiz
Nr.: 20181122
Zielformulierung des Teilprojekts Controller-software für ESR-BB
Name: M. Frey
Nach Projektende soll ein Controller von National Instruments die Anlagensteuerung fur beide Kavitäten mittels eines Labview-Programms ("VI") übernehmen.
Diese Software soll möglichst hohe Ausfallsicherheit und damit hohe Anlagenverfügbarkeit sicherstellen.
Die Software soll mit der bereits vorhandenen Hardware getestet sein und kompatibel mit der SPS-Anlagensteuerung sowie dem Beschleuniger-Kontrollsystem sein.

Related documents and information
=================================-
- README.md
- Release_Notes.md
- EUPL v.1.1 - Lizenz.pdf & EUPL v.1.1 - Lizenz.rtf
- Contact: your email
- Download, bug reports... : Git Repository URL
- Documentation:
  - Refer to Documantation folder 
  - NI Actor Framework: https://ni.com/actorframework
  - CS++
	- https://git.gsi.de/EE-LV/CSPP/CSPP/wikis/home
    - https://git.gsi.de/EE-LV/CSPP/CSPP_Documentation
  
Included Submodules
===================
- [Packages/CSPP_Core](https://git.gsi.de/EE-LV/CSPP/CSPP_Core): This package is used as submodule.
- [Packages/CSPP_ObjectManager](https://git.gsi.de/EE-LV/CSPP/CSPP_ObjectManager): This package is used as submodule.
- [Packages/CSPP_DSC](https://git.gsi.de/EE-LV/CSPP/CSPP_DSC): Containing DSC Alarm- & Trend-Viewer
- [Packages/CSPP_Utilities](https://git.gsi.de/EE-LV/CSPP/CSPP_Utilities): Providing some usefull utility classes. 

Optional Submodules
-------------------
- [Packages/CSPP_DeviceBase](https://git.gsi.de/EE-LV/CSPP/CSPP_DeviceBase): Definition of CS++Device ancestor classes
- [Packages/CSPP_IVI](https://git.gsi.de/EE-LV/CSPP/CSPP_IVI): Implementations of derived CS++Device classes using IVI driver
- [Packages/CSPP_LNA](https://git.gsi.de/EE-LV/CSPP/CSPP_LNA): Extends the Linked Network Actor to support zero coupled messages.
- [Packages/CSPP_RT](https://git.gsi.de/EE-LV/CSPP/CSPP_RT): Providing a librarie supporting LabVIEW-RT features. 
- [Packages/CSPP_PVConverter](https://git.gsi.de/EE-LV/CSPP/CSPP_PVConverter): Providing support for e.g. log-scaling of PV or conversion to array. 
- [Packages/CSPP_Syslog](https://git.gsi.de/EE-LV/CSPP/CSPP_Syslog): Providing a Syslog based Message Handler 
- [Packages/CSPP_Vacuum](https://git.gsi.de/EE-LV/CSPP/CSPP_Vacuum): Providing a DeviceActors dealing with vacuum. 

Refer to https://git.gsi.de/EE-LV/CSPP for more available CS++ submodules.

Optional External Dependencies
=================================
- Monitored Actor; Refer to
  - https://decibel.ni.com/content/thread/18301 and
  - http://lavag.org/topic/17056-monitoring-actors

Start Implementing ESRBB
===================================
- Extend `ESRBB.lvproj` to your needs.
  - Edit copyright information in description of `ESRBB.lvproj` and README.md
  - Use existing libraries and actors
    - Extend the `ESRBB.ini` with additiional entries.
	- You could also start your own ini-file with `CSPP_Minimum.ini`. You need to rename it to `ESRBB.ini` or to specify it in you main.vi or as command-line paramater.
  - Add more submodules
    - Template configuration ini-files and Shared Variable libraries should be included in all submodules.
    - Add more actors to configuration ini-file by copying from templates and renaming.
    - You need to create and deploy your project specific shared variable libraries.
         - e.g. copy Shared Variables from `CSPP_Core_SV.lvlib` to `ESRBB.lvlib` and rename.
  - Develop your project specific actor classes.
    - Provide template configuration ini-files and Shared Variable libraries
  - If Data Logging and Supervisory Control Module is available
    - Configure DSC settings, alarming and trending, for Shared Variables and libraries.
  - Optional: You can use a copy of `Packages\CSPP_Core\CSPP_DeploymentExample.vi` as starting point for your own application deployment.

Known issues:
=============

Author: H.Brand@gsi.de

Copyright 2021  GSI Helmholtzzentrum für Schwerionenforschung GmbH

EEL, Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.