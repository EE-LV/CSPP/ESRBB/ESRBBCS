Release Notes for the ESRBB Project
===========================================
This LabVIEW project _ESRBB.lvproj_ is used to develop the applications based on NI ActorFramework and CS++ libraries.

Version 0.0.0.0 26-04-2021 H.Brand@gsi.de
--------------------------------------
The ESRBB project was just forked. There is the master branch with some submodules, only.

